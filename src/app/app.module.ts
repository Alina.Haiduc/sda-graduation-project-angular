import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {MenuComponent} from './menu/menu.component';
import {HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import {FormsModule} from '@angular/forms';
import {AllRequestsModule} from './modules/all-requests/all-requests.module';
import {NewRequestModule} from './modules/new-request/new-request.module';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AllRequestsModule,
    NewRequestModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
