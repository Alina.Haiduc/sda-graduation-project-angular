import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequestsComponent} from './modules/all-requests/components/requests/requests.component';
import {NewRequestComponent} from './modules/new-request/components/new-request/new-request.component';

const routes: Routes = [
  {path: 'requests', component: RequestsComponent},
  {path: 'new-request', component: NewRequestComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
