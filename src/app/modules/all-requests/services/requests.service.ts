import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RequestType} from '../types/RequestType';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private http: HttpClient) {
  }

  public getAllDto(): Observable<RequestType[]> {
    return this.http.get<RequestType[]>('http://localhost:8090/api/v1/assignments/getAllDto');
  }

}
