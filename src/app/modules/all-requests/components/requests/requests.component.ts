import {Component, OnInit, ViewChild} from '@angular/core';
import {RequestType} from '../../types/RequestType';
import {RequestsService} from '../../services/requests.service';
import {Table} from 'primeng/table';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {

  requests: RequestType[];
  request: RequestType;
  selectedRequest: RequestType;
  cols: any[];
  requestDialog: boolean;
  submitted: boolean;

  @ViewChild('dt') table: Table;

  constructor(private requestsService: RequestsService) {
  }

  ngOnInit(): void {
    this.getAllDto();
  }

  public getAllDto(): void {
    this.requestsService.getAllDto().subscribe(req => this.requests = req);
    this.cols = [
      {field: 'assignmentId', header: 'Assignment Id'},
      {field: 'employeeName', header: 'Employee Name'},
      {field: 'status', header: 'Status'},
      {field: 'importance', header: 'Importance'}
    ];
  }

  editRequest(request: RequestType): void {
    this.request = {...request};
    this.requestDialog = true;
  }

}
