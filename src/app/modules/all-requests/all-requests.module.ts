import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AllRequestsRoutingModule} from './all-requests-routing.module';
import {RequestsComponent} from './components/requests/requests.component';
import {FormsModule} from '@angular/forms';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {RippleModule} from 'primeng/ripple';

@NgModule({
  declarations: [
    RequestsComponent
  ],
  exports: [
    RequestsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TableModule,
    DropdownModule,
    DialogModule,
    BrowserModule,
    BrowserAnimationsModule,
    RippleModule,
    AllRequestsRoutingModule
  ],
})
export class AllRequestsModule {
}
