export class RequestType {
  constructor(
    public assignmentId: number,
    public employeeName: string,
    public status: string,
    public importance: string
  ) {
  }
}
