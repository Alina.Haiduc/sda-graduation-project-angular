import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewRequestService {

  constructor(private http: HttpClient) {
  }

  public createDtoWithUser(employeeName: string, status: string, importance: string, userId: number): void {
    const params = new HttpParams().set('userId', String(userId));
    this.http.post('http://localhost:8090/api/v1/assignments/createWithUser', {
      employeeName,
      status,
      importance
    }, {params}).subscribe();
  }


}
