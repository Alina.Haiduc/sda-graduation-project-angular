import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewRequestComponent} from './components/new-request/new-request.component';
import {NewRequestRoutingModule} from './new-request-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    NewRequestComponent
  ],
  exports: [
    NewRequestComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NewRequestRoutingModule
  ],

})
export class NewRequestModule {
}
