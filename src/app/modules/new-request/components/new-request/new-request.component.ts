import {Component, OnInit} from '@angular/core';
import {NewRequestService} from '../../services/new-request.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-new-request',
  templateUrl: './new-request.component.html',
  styleUrls: ['./new-request.component.css']
})
export class NewRequestComponent implements OnInit {

  constructor(public newRequestService: NewRequestService) {
  }

  ngOnInit(): void {
  }

  public onSubmit(requestForm: NgForm): void {
    this.newRequestService.createDtoWithUser(
      requestForm.value.employeeName,
      requestForm.value.status,
      requestForm.value.importance,
      requestForm.value.userId);

      // 'Popa Oana',
      // 'Approved',
      // 'Low',
      // 1);
  }

}
