export class NewRequestType {
  constructor(
    public assignmentId: number,
    public employeeName: string,
    public status: string,
    public importance: string,
    public userId: number

  // public assignmentType: string,
  // public assignmentHostCountry: string,
  // public assignmentHostCity: string,
    // public homeCountry: string,
    // public homeCity: string,
    // public currentCity: string,
    // public progress: number,
    // public description: string,
    // public creationDate: any,
    // public lunchDate: any,
    // public completionDate: any,
    // public employeePersonId: string,
    // public assignmentDuration: string,
    // public position: string,
    // public newSupervisor: string,
    // public workCity: string,
    // public currentPca: number,
    // public currentPcp: number,
    // public newPca: number,
    // public newPcp: number,
    // public performance: number,
    // public familyGroup: string,
    // public familyGroupTravelling: string,
    // public startingDate: any,
    // public endingDate: any,
    // public assignmentReason: string,
    // public comments: string,
  ) {
  }
}
